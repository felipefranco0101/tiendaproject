package com.project.tienda.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT", schema = "TIENDA")
public class Product implements Serializable {

	private static final long serialVersionUID = -198640636091652176L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "PRICE")
	private Long price;
	
	@Column(name = "QUANTITY")
	private Long quantity;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_TYPE_PRODUCT", referencedColumnName = "ID")
	private TypeProduct typeProduct;

	public Product() {
	}

	public Product(Long id, TypeProduct typeProduct, String name, Long price, Long quantity) {
		this.id = id;
		this.typeProduct = typeProduct;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeProduct getTypeProduct() {
		return typeProduct;
	}

	public void setTypeProduct(TypeProduct typeProduct) {
		this.typeProduct = typeProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}
