package com.project.tienda.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PERSON", schema = "TIENDA")
public class Person implements Serializable {

	private static final long serialVersionUID = 2005208371535818346L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "BIRTHDATE")
	@Temporal(TemporalType.DATE)
	private Date birthdate;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "IDENTIFICATION")
	private Long identification;

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENT_TYPE")
	private DocumentType documentType;
	
	@OneToOne
	@JoinColumn(name = "ID_KIND_PERSON", referencedColumnName = "ID")
	private KindPerson kindPerson;

	public Person() {
	}

	public Person(String name, String email, Date birthdate, String phone, Long identification,
			DocumentType documentType, KindPerson kindPerson) {
		this.name = name;
		this.email = email;
		this.birthdate = birthdate;
		this.phone = phone;
		this.identification = identification;
		this.documentType = documentType;
		this.kindPerson = kindPerson;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getIdentification() {
		return identification;
	}

	public void setIdentification(Long identification) {
		this.identification = identification;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public KindPerson getKindPerson() {
		return kindPerson;
	}

	public void setKindPerson(KindPerson kindPerson) {
		this.kindPerson = kindPerson;
	}

}
