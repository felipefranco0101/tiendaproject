package com.project.tienda.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PRODUCT_SALES", schema = "TIENDA")
public class ProductSales implements Serializable {

	private static final long serialVersionUID = 3298151687422845149L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "QUANTITY")
	private Long quantity;

	@Column(name = "DATE_SALE")
	@Temporal(TemporalType.DATE)
	private Date dateSale;

	private Product product;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_SELLER", referencedColumnName = "ID")
	private Person seller;

	public ProductSales() {
	}

	public ProductSales(Long id, Long quantity, Date dateSale, Product product, Person seller) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.dateSale = dateSale;
		this.product = product;
		this.seller = seller;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Date getDateSale() {
		return dateSale;
	}

	public void setDateSale(Date dateSale) {
		this.dateSale = dateSale;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Person getSeller() {
		return seller;
	}

	public void setSeller(Person seller) {
		this.seller = seller;
	}

}
