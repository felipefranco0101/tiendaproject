package com.project.tienda.vo;

import java.util.Date;

import com.project.tienda.entity.DocumentType;
import com.project.tienda.entity.KindPerson;

public class PersonVO {

	private Long id;

	private String name;

	private String email;

	private String birthdate;

	private String phone;

	private Long identification;

	private DocumentType documentType;

	private KindPerson kindPerson;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getIdentification() {
		return identification;
	}

	public void setIdentification(Long identification) {
		this.identification = identification;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public KindPerson getKindPerson() {
		return kindPerson;
	}

	public void setKindPerson(KindPerson kindPerson) {
		this.kindPerson = kindPerson;
	}

}
