package com.project.tienda.vo;

import com.project.tienda.entity.Person;

public class ClientVO {

	private Long id;

	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
