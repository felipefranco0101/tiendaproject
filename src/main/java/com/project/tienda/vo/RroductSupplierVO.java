package com.project.tienda.vo;

import java.util.ArrayList;
import java.util.List;

import com.project.tienda.entity.Person;
import com.project.tienda.entity.Product;

public class RroductSupplierVO {

	private Long id;

	private List<Product> product = new ArrayList<>();

	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
