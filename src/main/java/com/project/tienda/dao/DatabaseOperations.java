package com.project.tienda.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.tienda.entity.Person;
import com.project.tienda.util.HibernateUtil;

public class DatabaseOperations {

	private static Transaction transaction;
	private static Session session = HibernateUtil.getSessionFactory().openSession();

	public void saveCliente(Person person) {
		try {
			transaction = session.beginTransaction();
			session.save(person);

		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			transaction.commit();
		}
	}

	public static <T> List<T> getAll(Class<T> type) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> criteria = (CriteriaQuery<T>) builder.createQuery(type);
		Root<T> entityClass = criteria.from(type);
		criteria.select(entityClass);
		List<T> data = session.createQuery(criteria).getResultList();
		return data;
	}
}
