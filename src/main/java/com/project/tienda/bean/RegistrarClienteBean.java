package com.project.tienda.bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.project.tienda.dao.DatabaseOperations;
import com.project.tienda.entity.DocumentType;
import com.project.tienda.entity.KindPerson;
import com.project.tienda.entity.Person;
import com.project.tienda.vo.PersonVO;

@ManagedBean
@RequestScoped
public class RegistrarClienteBean {

	private List<KindPerson> lstKindPerson;
	private KindPerson kindPerson;
	private List<DocumentType> lstDocumentType;
	private DocumentType documentType;
	private Person person;
	private PersonVO personVo;
	private DatabaseOperations operations;

	@PostConstruct
	public void init() {
		kindPerson = new KindPerson();
		lstKindPerson = DatabaseOperations.getAll(KindPerson.class);

		documentType = new DocumentType();
		lstDocumentType = DatabaseOperations.getAll(DocumentType.class);

		person = new Person();
		personVo = new PersonVO();

		operations = new DatabaseOperations();
	}

	public void guardarCliente() {
		person.setName(personVo.getName());
		person.setEmail(personVo.getEmail());
		person.setBirthdate(new Date());
		person.setPhone(personVo.getPhone());
		person.setDocumentType(documentType);
		person.setIdentification(personVo.getIdentification());
		person.setKindPerson(kindPerson);
		operations.saveCliente(person);

	}

	public List<KindPerson> getLstKindPerson() {
		return lstKindPerson;
	}

	public void setLstKindPerson(List<KindPerson> lstKindPerson) {
		this.lstKindPerson = lstKindPerson;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public KindPerson getKindPerson() {
		return kindPerson;
	}

	public void setKindPerson(KindPerson kindPerson) {
		this.kindPerson = kindPerson;
	}

	public List<DocumentType> getLstDocumentType() {
		return lstDocumentType;
	}

	public void setLstDocumentType(List<DocumentType> lstDocumentType) {
		this.lstDocumentType = lstDocumentType;
	}

	public PersonVO getPersonVo() {
		return personVo;
	}

	public void setPersonVo(PersonVO personVo) {
		this.personVo = personVo;
	}

}
