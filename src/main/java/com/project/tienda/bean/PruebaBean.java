package com.project.tienda.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.tienda.entity.KindPerson;
import com.project.tienda.util.HibernateUtil;

@ManagedBean
@SessionScoped
public class PruebaBean {

	private static Transaction transaction;
	private static Session session; 
	
	private String nombre = "Felipe";
	
	@PostConstruct
	public void init() {
		session = HibernateUtil.getSessionFactory().openSession();
	}
	
	public void savePersonKind() {
		try {
			KindPerson kindPerson = new KindPerson(getNombre());
			transaction = session.beginTransaction();
			session.save(kindPerson);
		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			transaction.commit();
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
